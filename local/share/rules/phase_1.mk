# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>

context prj/variants

sequences.fasta:
	zcat <$(REFERENCE) >$@

.META: nr.tab
	1	qacc	Query accesion
	2	qlen	Query sequence length
	3	sacc	Subject accession
	4	slen	Subject sequence length
	5	evalue	Expect value
	6	qstart	Start of alignment in query
	7	qend	End of alignment in query
	8	sstart	Start of alignment in subject
	9	send	End of alignment in subject
	10	length	Alignment length

nr.tab:
	ln -sf $(NR_BLAST_TAB) $@

orf.bed:
	ln -sf $(ORF_PRED_FASTA) $@

# prepare misa config file

# 	In a single line beginning with "def" a sequence of number pairs is expected,
#       whereas the first number defines the unit sizes and the second number the 
#       lower threshold of repeats for that specific unit.

# 	In a single line beginning with "int" a single number is expected defining 
#       the maximal number of bases between two adjacent microsatellites to be 
#       recognised as being a compound microsatellite.
misa.ini:
	> $@; \
	printf "definition(unit_size,min_repeats):\t$(MISA_DEFINITION)\n" >> $@; \
	printf "interruptions(max_difference_between_2_SSRs):\t$(MISA_INTERRUPTIONS)\n" >> $@;

# MISA

# Results of the microsatellite search are stored in two files:

#     1) "<FASTfile>.misa" the localization and type of identified microsatellite(s) are stored in a tablewise manner.
#     2) "<FASTfile>.statistics" summarizes different statistics as the frequency of a specific microsatellite type according to the unit size or individual motifs.
#        Frequency of identified SSR motifs: repetition frequency
#        Frequency of classified repeat types (considering sequence complementary):
#        repetition frequency considering repetition, its reverse complementary, in one
#        in one strand and the other: AC/GT + CA/TG
all.misa.ssr: sequences.fasta misa.ini
	misa $< $^2 \
	&& mv $<.misa $@ \
	&& mv $<.statistics $@.statistic

all.misa.ssr.statistics: all.misa.ssr
	touch $@

misa.ssr: all.misa.ssr
	bawk '!/^[\#+,$$]/ { \
	if ( $$3 !~ /c/ ) print $$0; \
	}' $< >$@

# TANDEM REPEAT FINDER (TRF)
tmp.trf.ssr: sequences.fasta
	trf409 $< $(TRF_PARAM) -h -ngs >$@

# transform TRF outuput into MISA output
# TRINITY_DN10074_c0_g1_i1	1	p3	(AAT)9	27	65	91
trf.ssr: tmp.trf.ssr
	tr '@' '>' <$<\
	| fasta2tab -m \
	| bsort -k 1,1 \
	| tr ' ' '\t' \
	| sed -e 's/\t/:/' -e 's/\t/ /g' -e 's/:/\t/' \   * transform all but the first tab into sapce *
	| collapsesets -o 2 \   * all rows of a same scaffold are collapsed in unique row *
	| bawk 'BEGIN { print "ID", "SSR nr.", "SSR type", "SSR", "size", "start", "end"; } \
	!/^[\#+,$$]/ { \
	split($$2,line,";"); \
	for ( i=1; i<=length(line); i++ ) { \   * calculate number of contigs per scaffold *
		split(line[i], item, " "); \
		if ( item[3] > 1 ) { \   * remove single base repeats *
			printf "%s\t%i\tp%i\t(%s)%.1f\t%i\t%i\t%i\n", $$1, i, item[3], item[14], item[4], length(item[15]), item[1], item[2]; \
		} \
	} \
	}' >$@


# select columns from blast result of contings against NCBI nr
# selected fileds are: seq_ID, query_start, query_end
nr_hit_dict.tab: nr.tab
	bawk '!/^[$$,\#+]/ { \
	if ( $$qstart < $$qend ) { \
		printf "%s\t%i\t%i\n", $$qacc, $$qstart, $$qend; \
	} \
	else { \
		printf "%s\t%i\t%i\n", $$qacc, $$qend, $$qstart; } \
	}' $< >$@


# from fasta file of predicted orf from sequences that do not have a match
# against NCBI nr, I estract seq_ID, ORF_start, ORF_end, strand
orf_dict.tab: orf.bed
	bawk '!/^[$$,\#+]/ { \
	if ($$10 < $$11) { \
		printf "%s\t%i\t%i\n", $$1, $$10, $$11; \
	} else \
		printf "%s\t%i\t%i\n", $$1, $$11, $$10; \
	}' $< >$@


# List of putative ORF coordinate for SSR-containing contigs

# For each contig containing SSRs the coordinates of putative orf contained in it are added.
# The ORFS can come from BLAST or prediction. If there is no ORF coordinates will be 0 0.
# Check that the coordinates are compatible with the length of the contig.
%.results.orf.tab: %.ssr nr_hit_dict.tab orf_dict.tab sequences.fasta
	sed '1d' $< \
	| cut -f1 \
	| bsort \
	| uniq \
	| translate -a -v $^2 1 \   * add corresponding coordinate from blast *
	| translate -a -v $^3 1 \   * add corresponding coordinate from prediction *
	| translate -a -v <(fasta_length $^4) 1 \   * add lenghts *
	| bawk 'function abs(x) {return ((x < 0.0) ? -x : x)} !/^[$$,\#+]/ { \
		if ( $$5 != "" && $$6 != "" && abs($$6-$$5) <= $$2 ) { print $$1, $$2, $$5, $$6; } \   * check that the coordinates indicate actually a region internal to the sequence *
		else { \
			if ( $$3 != "" && $$4 != "" && abs($$4-$$3) <= $$2 ) { print $$1, $$2, $$3, $$4; } \   * Some sequences may have no orf. Kill them *
		} \
	}' \
	| sed 1'i\#SeqI\tSeqLength\tmin_coordinate\tmax_coordinate' >$@   * add column names *





# List if overlap fractions for SSR in SSR-containing contigs with ORFs
# merges the coordinates of the ORFs to those of the SSRs and calculates the fraction of overlap between ORF regions and SSR regions.
%.results.overlaps.tab: %.ssr %.results.orf.tab
	sed '1d' $< \
	| bsort --key=1,1 \
	| translate -a -k <(sed '1d' $^2 | bsort --key=1,1) 1 \
	| select_columns 3 4 9 10 1 5 \   * min_orf_coord max_orf_coord min_SSR_coord max_SSR_coord contig SSR_num *
	| region_overlap --already-sorted >$@

%.results.overlaps.stat: %.results.overlaps.tab
	bawk 'BEGIN {complete=0; partially=0; total=0; \
	print "SSR_completely_in_ORFs","SSR_partially_in_ORFs","total_SSR_in_SSR-containing_contigs_with_ORF"; \
	} !/^[$$,\#+]/ {  \
	if ($$3 == 1 ) { complete++; } \
	if ($$3 < 1 && $$3 > 0) { partially++; } \
	total++; } \
	END { print complete, partially, total; }' <$< >$@


.META: class.distribution.stat
	1	unit size
	2	trf number of SSRs
	3	misa number of SSRs

class.distribution.stat: misa.ssr trf.ssr
	paste \
	<(unhead <$< \
	| bawk '!/^[$$,\#+]/ { sub("p","",$$3); if ( $$3 != "" ) print $$3 }' \
	| bsort \
	| uniq -c \
	| tr -s ' ' \\t \
	| sed -e 's/^[ \t]*//' \
	| select_columns 2 1) \
	| translate -a -v \
	<(unhead <$^2 \
	| bawk '!/^[$$,\#+]/ { sub("p","",$$3); if ( $$3 != "" ) print $$3 }' \
	| bsort \
	| uniq -c \
	| tr -s ' ' \\t \
	| sed -e 's/^[ \t]*//' \
	| select_columns 2 1) 1 >$@




common.trf.misa: sequences.fasta misa.ssr trf.ssr
	fasta2tab <$< \
	| cut -f1 \
	| translate -v -a \
	<(unhead $^2 \
	| bsort -k 1,1 \
	| tr ' ' '\t' \
	| sed -e 's/\t/:/' -e 's/\t/ /g' -e 's/:/\t/' \   * transform all but the first tab into sapce *
	| collapsesets -o 2) \   * all rows of a same scaffold are collapsed in unique row *
	1 \
	| translate -v -a \
	<(unhead $^3 \
	| bsort -k 1,1 \
	| tr ' ' '\t' \
	| sed -e 's/\t/:/' -e 's/\t/ /g' -e 's/:/\t/' \
	| collapsesets -o 2) \
	1 \
	| bawk '{ if ( $$2 != "" || $$3 != "" ) print $$0; }' >$@


common.trf.misa.stat: common.trf.misa
	bawk 'BEGIN {common=0; first_only=0; second_only=0; total=0; \
	print "common", "first_only", "second_only", "total"; \
	} !/^[$$,\#+]/ {  \
	if ( $$2 != "" && $$3 != "" ) { common++; } \
	if ( $$2 != "" && $$3 == "" ) { first_only++; } \
	if ( $$2 == "" && $$3 != "" ) { second_only++; } \
	total++; } \
	END { print common, first_only, second_only, total; }' <$< >$@


ALL += misa.results.orf.tab \
	misa.results.overlaps.tab \
	misa.results.overlaps.stat \
	trf.results.orf.tab \
	trf.results.overlaps.tab \
	trf.results.overlaps.stat \
	misa.results.orf.tab \
	misa.results.overlaps.tab \
	misa.results.overlaps.stat \
	common.trf.misa.stat


INTERMEDIATE += 


CLEAN += trf.ssr \
	tmp.trf.ssr \
	trf.results.orf.tab \
	trf.results.overlaps.tab \
	trf.results.overlaps.stat \
	common.trf.misa \
	common.trf.misa.stat